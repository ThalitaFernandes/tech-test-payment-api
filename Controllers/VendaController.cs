using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly VendaContext _context;

        public VendaController(VendaContext context)
        {
            _context = context;
        }

        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var venda = _context.Vendas.Find(id);

            if(venda == null)
                return NotFound();
           
            return Ok(venda);
        }

        [HttpPost]
        public IActionResult Criar(Venda venda)
        {
            _context.Add(venda);
            _context.SaveChanges();
            return CreatedAtAction(nameof(ObterPorId), new { id = venda.Id }, venda);
        }

        [HttpPut("{id}")]
        public IActionResult Atualizar(int id, Venda venda)
        {
            var vendaBanco = _context.Vendas.Find(id);

            if (vendaBanco == null)
                return NotFound();
       
            vendaBanco.Vendedor = venda.Vendedor;
            vendaBanco.Pedidos = venda.Pedidos;
            vendaBanco.Data = venda.Data;
            var status = vendaBanco.Status;
            if(status == EnumStatusVenda.Aguardando_pagamento)
                if(status != EnumStatusVenda.Aguardando_pagamento || status != EnumStatusVenda.Cancelada)
                    return NoContent();
            
            if(status == EnumStatusVenda.Pagamento_aprovado)
                if(status != EnumStatusVenda.Enviado_para_transportadora || status != EnumStatusVenda.Cancelada)
                    return NoContent();

            if(status == EnumStatusVenda.Enviado_para_transportadora)
                if(status != EnumStatusVenda.Entregue)
                    return NoContent();
            
            vendaBanco.Status = venda.Status;

            _context.Vendas.Update(vendaBanco);
            _context.SaveChanges();
            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult Deletar(int id)
        {
            var vendaBanco = _context.Vendas.Find(id);

            if (vendaBanco == null)
                return NotFound();

            _context.Vendas.Remove(vendaBanco);
            _context.SaveChanges();

            return NoContent();
        }
    }
}
