
namespace tech_test_payment_api.Entities
{
    public enum EnumStatusVenda
    {
        Aguardando_pagamento,
        Pagamento_aprovado,
        Enviado_para_transportadora,
        Entregue,
        Cancelada
    }
}